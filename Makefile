.PHONY: install
install:
	docker run --rm --name fbhelper_node -it -v `pwd`:/app -w /app node yarn

.PHONY: build
build:
	docker run --rm --name fbhelper_node -it -v `pwd`:/app -w /app node yarn run build

.PHONY: watch
watch:
	docker run --rm --name fbhelper_node -it -v `pwd`:/app -w /app node yarn run watch

.PHONY: yarn
yarn:
	docker run --rm --name fbhelper_node -it -v `pwd`:/app -w /app -p 5000:5000 node yarn $(c)
